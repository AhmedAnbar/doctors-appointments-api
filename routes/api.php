<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([ 'middleware' => 'api', 'prefix' => 'auth' ], function ($router) {
    Route::post('login', 'AuthController@login');
});
Route::middleware(['auth:api'])->get('auth/logout', function (Request $request) {
    $request->user()->token()->revoke();
});

Route::middleware(['auth:api'])->get('auth/check', function (Request $request) {
    $request->user()->token();
});

Route::group([ 'middleware' => 'auth:api', 'prefix' => 'users' ], function ($router) {
  Route::get('list', 'UsersController@index');
  Route::get('{id}', 'UsersController@show');
  Route::post('create', 'UsersController@store');
  Route::put('{id}', 'UsersController@update');
  Route::delete('{id}', 'UsersController@destroy');
});

Route::group([ 'middleware' => 'auth:api', 'prefix' => 'types' ], function ($router) {
  Route::get('list', 'TypeController@index');
  Route::get('{id}', 'TypeController@show');
  Route::post('create', 'TypeController@store');
  Route::put('{id}', 'TypeController@update');
  Route::delete('{id}', 'TypeController@destroy');
});

Route::group([ 'middleware' => 'auth:api', 'prefix' => 'appointments' ], function ($router) {
  Route::post('list', 'AppointmentController@index');
  Route::get('{id}', 'AppointmentController@show');
  Route::post('create', 'AppointmentController@store');
  Route::put('{id}', 'AppointmentController@update');
  Route::delete('{id}', 'AppointmentController@destroy');
});

Route::group([ 'middleware' => 'auth:api', 'prefix' => 'patients' ], function ($router) {
  Route::get('list', 'PatientController@index');
  Route::get('{id}', 'PatientController@show');
  Route::post('create', 'PatientController@store');
  Route::put('{id}', 'PatientController@update');
  Route::delete('{id}', 'PatientController@destroy');
});

Route::group([ 'middleware' => ['auth:api', 'Localization'], 'prefix' => 'statuses' ], function ($router) {
  Route::get('list', 'StatusController@index');
  Route::get('{id}', 'StatusController@show');
  Route::post('create', 'StatusController@store');
  Route::put('{id}', 'StatusController@update');
  Route::delete('{id}', 'StatusController@destroy');
});
