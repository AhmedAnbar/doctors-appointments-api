<?php

return [
    'pending' => 'مؤجل',
    'completed' => 'مكتمل',
    'canceled' => 'ملغي',
];