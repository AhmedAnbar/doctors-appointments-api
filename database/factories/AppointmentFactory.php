<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Appointment;
use Faker\Generator as Faker;

$factory->define(Appointment::class, function (Faker $faker) {
		$end_date = $faker->dateTime();
		$start_date = $faker->dateTime($max=$end_date);
    return [
    	'start_date' => $start_date,
    	'end_date' => $end_date,
    	'patient_id' => rand(1, 5),
    	'doctor_id' => rand(1,10),
    	'status_id' => 1,
    ];
});
