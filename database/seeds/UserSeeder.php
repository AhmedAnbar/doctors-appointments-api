<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'name' => 'Ahmed Moahmed',
	        'email' => 'ahmed@company.com',
	        'email_verified_at' => now(),
	        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
	        'type_id' => 1,
	        'remember_token' => Str::random(10),
        ]);

        User::create([
        	'name' => 'Ali Ahmed',
	        'email' => 'ali@company.com',
	        'email_verified_at' => now(),
	        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
	        'type_id' => 1,
	        'remember_token' => Str::random(10),
        ]);

        User::create([
        	'name' => 'Mohamed Ahmed',
	        'email' => 'mohamed@company.com',
	        'email_verified_at' => now(),
	        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
	        'type_id' => 2,
	        'remember_token' => Str::random(10),
        ]);
    }
}
