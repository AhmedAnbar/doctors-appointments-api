<?php

use App\Patient;
use Illuminate\Database\Seeder;

class PatientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Patient::create([
        	'name' => 'Emad Ali',
        	'address' => '1 street, city, country',
        	'phone' => '01234567'
        ]);
        Patient::create([
        	'name' => 'Ahmed Ali',
        	'address' => '1 street, city, country',
        	'phone' => '01234567'
        ]);
        Patient::create([
        	'name' => 'Mahmoud Mostafa',
        	'address' => '1 street, city, country',
        	'phone' => '01234567'
        ]);
        Patient::create([
        	'name' => 'Said Hassan',
        	'address' => '1 street, city, country',
        	'phone' => '01234567'
        ]);
        Patient::create([
        	'name' => 'Sayed Anber',
        	'address' => '1 street, city, country',
        	'phone' => '01234567'
        ]);
        Patient::create([
        	'name' => 'Moahmed Nasr',
        	'address' => '1 street, city, country',
        	'phone' => '01234567'
        ]);
        Patient::create([
        	'name' => 'Magdy Hassan',
        	'address' => '1 street, city, country',
        	'phone' => '01234567'
        ]);
        Patient::create([
        	'name' => 'Yasser Gaffar',
        	'address' => '1 street, city, country',
        	'phone' => '01234567'
        ]);
        Patient::create([
        	'name' => 'Ibrahem Ali',
        	'address' => '1 street, city, country',
        	'phone' => '01234567'
        ]);
        Patient::create([
        	'name' => 'Ahmed Samy',
        	'address' => '1 street, city, country',
        	'phone' => '01234567'
        ]);
    }
}
