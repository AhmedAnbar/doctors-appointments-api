<?php

namespace App\Http\Requests\Appointment;

use Illuminate\Foundation\Http\FormRequest;

class updateAppointmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => 'nullable|date_format:"Y-m-d H:i:s"',
            'end_date' => 'nullable|date_format:"Y-m-d H:i:s"',
            'patient_id' => 'nullable|integer|exists:patients,id',
            'doctor_id' => 'nullable|integer|exists:users,id',
            'status_id' => 'nullable|integer|exists:statuses,id',
        ];
    }
}
