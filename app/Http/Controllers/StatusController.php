<?php

namespace App\Http\Controllers;

use App\Http\Requests\Appointment\addStatusRequest;
use App\Http\Requests\Appointment\updateStatusRequest;
use App\Http\Resources\Appointment\StatusCollection;
use App\Http\Resources\Appointment\StatusResource;
use App\Status;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    public function index()
    {
      try{
        $statuses = Status::latest()->paginate(30);
        return response()->json(['data' => new StatusCollection($statuses), 'message' => 'Statuses Retrived.']);
      } catch(Exception $e) {
        return response()->json(['message' => $e->getMessage()], 400);
      }
    }

    public function store(addStatusRequest $request)
    {
      $input = $request->validated();
      $input['created_at'] = Carbon::now();

      try{
        Status::create($input);
        return response()->json(['message' => 'Status Created']);
      } catch(Exception $e) {
         return response()->json(['message' => $e->getMessage()], 400);
      }
    }

    public function show($id)
    {
      $status = Status::find($id);
      
      if($status) {
        return response()->json(['data' => new StatusResource($status), 'message' => 'Status Retrived.']);
      } else {
         return response()->json(['message' => $e->getMessage()], 400);
      }
    }

    public function update(updateStatusRequest $request, $id)
    {
      $status = Status::find($id);
      $input = $request->validated();
      if($status) {
        $status->updated_at = Carbon::now();
        $status = $status->fill($input);
      }
      try{
        $status->save();
        return response()->json(['data' => new StatusResource($status), 'message' => "Status Updated"]);
      }
      catch(Exception $e) {
         return response()->json(['message' => $e->getMessage()], 400);
      }
    }

    public function destroy($id)
    {
      $status = Status::find($id);
      try{
        $status->delete();
        return response()->json(['message' => "Status Deleted"]);
      } catch(Exception $e) {
          return response()->json(['message' => $e->getMessage()], 400);
      }
    }
}
