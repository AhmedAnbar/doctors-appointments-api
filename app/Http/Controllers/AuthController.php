<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Resources\User\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(LoginRequest $request) {

    $loginData = $request->validated();

    if (!auth()->attempt($loginData)) {
        return response(['message' => 'Invalid Credentials']);
    }

    $accessToken = auth()->user()->createToken('authToken')->accessToken;

    return response(['user' => new UserResource(auth()->user()), 'access_token' => $accessToken]);
  }

  public function logout() {
    $user = Auth::user()->token();
    $user->revoke();
    return response(['message' => 'Logged out']); 
  }
}
