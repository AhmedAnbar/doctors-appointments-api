<?php

namespace App\Http\Controllers;

use App\Http\Requests\Patient\addPatientRequest;
use App\Http\Requests\Patient\updatePatientRequest;
use App\Http\Resources\Patient\PatientCollection;
use App\Http\Resources\Patient\PatientResource;
use App\Patient;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    public function index()
    {
      try{
        $patients = Patient::latest()->paginate(30);
        return response()->json(['data' => new PatientCollection($patients), 'message' => 'Patients Retrived.']);
      } catch(Exception $e) {
        return response()->json(['message' => $e->getMessage()], 400);
      }
    }

    public function store(addPatientRequest $request)
    {
      $input = $request->validated();
      $input['created_at'] = Carbon::now();

      try{
        Patient::create($input);
        return response()->json(['message' => 'Patient Created']);
      } catch(Exception $e) {
         return response()->json(['message' => $e->getMessage()], 400);
      }
    }

    public function show($id)
    {
      $patient = Patient::find($id);
      
      if($patient) {
        return response()->json(['data' => new PatientResource($patient), 'message' => 'Patient Retrived.']);
      } else {
         return response()->json(['message' => $e->getMessage()], 400);
      }
    }

    public function update(updatePatientRequest $request, $id)
    {
      $patient = Patient::find($id);
      $input = $request->validated();
      if($patient) {
        $patient->updated_at = Carbon::now();
        $patient = $patient->fill($input);
      }
      try{
        $patient->save();
        return response()->json(['data' => new PatientResource($patient), 'message' => "Patient Updated"]);
      }
      catch(Exception $e) {
         return response()->json(['message' => $e->getMessage()], 400);
      }
    }

    public function destroy($id)
    {
      $patient = Patient::find($id);
      try{
        $patient->delete();
        return response()->json(['message' => "Patient Deleted"]);
      } catch(Exception $e) {
          return response()->json(['message' => $e->getMessage()], 400);
      }
    }
}
