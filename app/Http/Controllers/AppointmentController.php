<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Http\Requests\Appointment\ListAppointmentRequest;
use App\Http\Requests\Appointment\addAppointmentRequest;
use App\Http\Requests\Appointment\updateAppointmentRequest;
use App\Http\Resources\Appointment\AppointmentCollection;
use App\Http\Resources\Appointment\AppointmentResource;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    public function index(ListAppointmentRequest $request)
    {
      $input = $request->validated();

      $appointments = Appointment::all();
      
      if(isset($input['search']) && $input['search'] == 'future') {
        $appointments = $appointments->where('start_date', '>=', date('Y-m-d'));
      } elseif(isset($input['search']) && $input['search'] == 'past') {
        $appointments = $appointments->where('start_date', '<=', date('Y-m-d'));
      } else {
      }

      if($appointments){
        return response()->json(['data' => new AppointmentCollection($appointments), 'message' => 'Appointments Retrived.']);
      } else {
        return response()->json(['message' => $e->getMessage()], 400);
      }
    }

    public function store(addAppointmentRequest $request)
    {
      $input = $request->validated();
      $input['created_at'] = Carbon::now();

      try{
        Appointment::create($input);
        return response()->json(['message' => 'Appointment Created']);
      } catch(Exception $e) {
         return response()->json(['message' => $e->getMessage()], 400);
      }
    }
    public function show($id)
    {
      $appointment = Appointment::find($id);
      
      if($appointment) {
        return response()->json(['data' => new AppointmentResource($appointment), 'message' => 'Appointment Retrived.']);
      } else {
         return response()->json(['message' => $e->getMessage()], 400);
      }
    }

    public function update(updateAppointmentRequest $request, $id)
    {
      $appointment = Appointment::find($id);
      $input = $request->validated();
      if($appointment) {
        $appointment->updated_at = Carbon::now();
        $appointment = $appointment->fill($input);
      }
      try{
        $appointment->save();
        return response()->json(['data' => new AppointmentResource($appointment), 'message' => "Appointment Updated"]);
      }
      catch(Exception $e) {
         return response()->json(['message' => $e->getMessage()], 400);
      }
    }

    public function destroy($id)
    {
      $appointment = Appointment::find($id);
      try{
        $appointment->delete();
        return response()->json(['message' => "Appointment Deleted"]);
      } catch(Exception $e) {
          return response()->json(['message' => $e->getMessage()], 400);
      }
    }
}
