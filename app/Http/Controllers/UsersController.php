<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\addUserRequest;
use App\Http\Requests\User\updateUserRequest;
use App\Http\Resources\User\UserCollection;
use App\Http\Resources\User\UserResource;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index()
    {
    	try{
        $users = User::latest()->paginate(30);
        return response()->json(['data' => new UserCollection($users), 'message' => 'Users Retrived.']);
      } catch(Exception $e) {
        return response()->json(['message' => $e->getMessage()], 400);
      }
    }

    public function store(addUserRequest $request)
    {
      $input = $request->validated();
      $input['password'] = Hash::make($input['password']);
      $input['created_at'] = Carbon::now();

      try{
        User::create($input);
        return response()->json(['message' => 'User Created']);
      } catch(Exception $e) {
         return response()->json(['message' => $e->getMessage()], 400);
      }
    }

    public function show($id)
    {
      $user = User::find($id);
      
      if($user) {
        return response()->json(['data' => new UserResource($user), 'message' => 'User Retrived.']);
      } else {
         return response()->json(['message' => $e->getMessage()], 400);
      }
    }

    public function update(updateUserRequest $request, $id)
    {
      $user = User::find($id);
      $input = $request->validated();
      if($user) {
        $user->updated_at = Carbon::now();
        if (isset($input['password'])) {
        	$input['password'] = Hash::make($input['password']);
        }
        $user = $user->fill($input);
      	// dd($request->all());
      }
      try{
        $user->save();
        return response()->json(['data' => new UserResource($user), 'message' => "User Updated"]);
      }
      catch(Exception $e) {
         return response()->json(['message' => $e->getMessage()], 400);
      }
    }

    public function destroy($id)
    {
      $user = User::find($id);
      try{
        $user->delete();
        return response()->json(['message' => "User Deleted"]);
      } catch(Exception $e) {
          return response()->json(['message' => $e->getMessage()], 400);
      }
    }
}
