<?php

namespace App\Http\Resources\Patient;

use Illuminate\Http\Resources\Json\JsonResource;

class PatientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "patientId" =>                  $this->id,
            "patientName" =>                $this->name,
            "patientAddress" =>             $this->address,
            "patientPhone" =>               $this->phone,
            // "created_at" =>          $this->created_at,
            // "updated_at" =>          $this->updated_at,
        ];
    }
}
