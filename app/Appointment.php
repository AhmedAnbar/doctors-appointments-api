<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start_date', 'end_date', 'patient_id', 'doctor_id', 'status_id',
    ];

    protected $table = "appointments";

    public function doctor() {
        return $this->belongsTo('App\User', 'id', 'doctor_id');
    }

    public function patient() {
        return $this->belongsTo('App\Patient');
    }

    public function status() {
        return $this->belongsTo('App\Status');
    }
}
